<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * Localized language
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ppbase' );

/** Database username */
define( 'DB_USER', 'lamp' );

/** Database password */
define( 'DB_PASSWORD', 'lamp' );

/** Database hostname */
define( 'DB_HOST', 'database' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '?ZI$[^~{>#uVNdT25!1:)&jJ5*D[piE,lPsQ^uyP09QtuB}HeFt$C+SX3`Wr-H+h' );
define( 'SECURE_AUTH_KEY',   '4n]THRJRX^`~OXe0{e>42N*/!y>2.KBOs{arMp{m1=zhkO(5tmM*Vp1%^JG<__eZ' );
define( 'LOGGED_IN_KEY',     'Y;CO7hw,w]V$=@L|xj6E^1C3|75I,*v6E2jeZ~]|0IXb`>>n2RA I[w#pI+>zGY3' );
define( 'NONCE_KEY',         '[L%E?[nl/R(Iwctliwz8A5wV8mplno8>3:[s#W9Ozci6~/0/v*}l`vowmYydtf4T' );
define( 'AUTH_SALT',         'eg9#=0$=T.``_$!o6Na1*l/:LbXS-Zjxay%BdoH2$-c,j3VY+P)`}p-4Gn!I*Lh|' );
define( 'SECURE_AUTH_SALT',  'BQlW]}K6pb93<%zpP9ZBD@]f+1YGju<=@*mQmG S8t>b{zRP4??d<-]i+84@=NYr' );
define( 'LOGGED_IN_SALT',    't2YB+Z(*r2/j26Ile^_T}2iRkt1o;nS)|9v_!LFv:m GV!|x5V8cO)H2 [&_,xvm' );
define( 'NONCE_SALT',        ')o&c_y{lB{k8D|7U=Fe6j.~A+eRA;Q#U.Hg qrGH*34k4?}X)5gVuhRs}bz<brh>' );
define( 'WP_CACHE_KEY_SALT', '5d*!qy>[G2.iDEFYn1^)O{*7`CT5*wqg&l[9;902r97BeYU*b0,(=ql>3a=gB44x' );


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );


/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
