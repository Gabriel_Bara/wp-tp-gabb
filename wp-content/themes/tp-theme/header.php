<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/wp-content/themes/tp-theme/style.css">
    <link rel="stylesheet" href="/wp-content/themes/tp-theme/reset.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>TP WP GB</title>
</head>

<body>
    <header>
        <div class="header-top">
            <div class="header-top-w">
                <div class="main-title">
                    <?php echo get_bloginfo("name "); ?>
                </div>
                <div class="icons">
                    <i class="fa-solid fa-rss"></i>
                    <i class="fa-brands fa-facebook-f"></i>
                    <i class="fa-brands fa-twitter"></i>
                    <i class="fa-brands fa-linkedin-in"></i>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="header-bottom-w">
                <?php wp_nav_menu() ?>
                <form>
                    <div>
                        <label for="article-research"></label>
                        <input type="search" id="article-research" name="search" placeholder="Recherche d'articles">
                        <button>🔍</button>
                    </div>
                </form>
            </div>
            <span class="header-bottom-bottom-blur"></span>
        </div>
    </header>
    <main>