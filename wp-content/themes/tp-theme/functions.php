<?php

function register_menu()
{
    register_nav_menus(
        array(
            'menu-sup' => 'Menu sup'
        )
    );
}

add_action('init', 'register_menu');
