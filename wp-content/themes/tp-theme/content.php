<article class="listed-article">
    <h4><?php the_title() ?></h4>
    <p>
        Par : <?php the_author() ?><br>
        Le : <?php the_date() ?><br>
    </p>
    <?php the_content() ?>
</article>