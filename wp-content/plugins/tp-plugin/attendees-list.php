<?php


require_once plugin_dir_path(__FILE__) . "/service/Tp_Db_Service.php";

class AttendeesList extends WP_List_Table
{
    private $dal;

    public function __construct($args = array())
    {
        parent::__construct([
            'singular' => __("Attendee"),
            'plural' => __("Attendees")
        ]);

        $this->dal = new Tp_Db_Service();
    }

    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        // pagination
        $perPage = $this->get_items_per_page('attendees_per_page', 10);
        $currentPage = $this->get_pagenum();

        //données
        $data = $this->dal->get_attendees_data();
        $totalPage = count($data);

        //tri
        usort($data, array(&$this, 'usort_reorder'));
        $paginationData = array_slice($data, (($currentPage - 1) * $perPage), $perPage);

        $this->set_pagination_args([
            'total-item' => $totalPage,
            'per-page' => $perPage
        ]);

        $this->_column_headers = [$columns, $hidden, $sortable];
        $this->items = $paginationData;
    }

    protected function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'id':
            case 'firstname':
            case 'lastname':
            case 'name':
            case 'email':
            case 'phone':
            case 'city':
                return $item->$column_name;
            case 'suppr':
                return '<form method="post"><input type="hidden" name="delete" value="' . $item->id . '"><input type="submit" value="❌"></form>';
                break;
            default:
                return print_r($item, true);
        }
    }

    public function usort_reorder($a, $b)
    {
        $orderBy = (!empty($_GET['orderby'])) ? $_GET['orderby'] : 'id';
        $order = (!empty($_GET['order'])) ? $_GET['order'] : 'desc';
        $result = strcmp($a->$orderBy, $b->$orderBy);
        return ($order === 'asc') ? $result : -$result;
    }

    public function get_columns()
    {
        $columns = [
            'id' => 'Num. adhérent',
            'firstname' => 'Prénom',
            'lastname' => 'Nom',
            'name' => 'Club',
            'email' => 'Email',
            'phone' => 'Téléphone',
            'city' => 'Ville',
            'suppr' => 'Supprimer'
        ];
        return $columns;
    }

    public function get_hidden_columns(): array
    {
        return [];
    }

    public function get_sortable_columns()
    {
        return [
            'id' => ['id', true],
            'firstname' => ['firstname', true],
            'lastname' => ['lastname', true],
            'name' => ['name', false],
            'email' => ['email', false],
            'phone' => ['phone', false],
            'city' => ['city', false]
        ];
    }
}
