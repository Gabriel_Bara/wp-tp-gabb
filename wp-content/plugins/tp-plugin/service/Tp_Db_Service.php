<?php

class Tp_Db_Service
{
    public function __construct()
    {
    }

    #region general

    public function create_db()
    {
        global $wpdb;

        $wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}tp_attendees` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `firstname` varchar(45) NOT NULL,
            `lastname` varchar(45) NOT NULL,
            `club_id` int(11) NOT NULL,
            `contact_id` int(11) NOT NULL,
            PRIMARY KEY (`id`))");

        $wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}tp_clubs` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(45) NOT NULL,
            `domain_id` int(11) NOT NULL,
            `contact_id` int(11) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `name_UNIQUE` (`name`))");

        $wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}tp_contact` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `email` varchar(45) NOT NULL,
            `phone` varchar(45) NOT NULL,
            `street` varchar(45) NOT NULL,
            `city` varchar(45) NOT NULL,
            `postal_code` varchar(45) NOT NULL,
            PRIMARY KEY (`id`))");

        $wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}tp_domains` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `domain_name` varchar(45) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `name_UNIQUE` (`domain_name`))");

        $wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}tp_tournament_results` (
            `tournament_id` int(11) NOT NULL AUTO_INCREMENT,
            `club_id` int(11) NOT NULL,
            `score` int(11) NOT NULL,
            PRIMARY KEY (`tournament_id`,`club_id`,`score`))");

        $wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}tp_tournaments` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(45) NOT NULL,
            `domain_id` varchar(45) DEFAULT NULL,
            PRIMARY KEY (`id`))");
    }

    public function find_all(string $table_name_without_prefix)
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}$table_name_without_prefix;");

        return $res;
    }

    public static function empty_db()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_attendees;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_clubs;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_contact;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_tournaments;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_tournament_results;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_domains;");
    }

    public static function drop_db()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE {$wpdb->prefix}tp_attendees;");
        $wpdb->query("DROP TABLE {$wpdb->prefix}tp_clubs;");
        $wpdb->query("DROP TABLE {$wpdb->prefix}tp_contact;");
        $wpdb->query("DROP TABLE {$wpdb->prefix}tp_tournaments;");
        $wpdb->query("DROP TABLE {$wpdb->prefix}tp_tournament_results;");
        $wpdb->query("DROP TABLE {$wpdb->prefix}tp_domains;");
    }

    /**
     * Sert à vérifier si un contact sur le point d'être envoyé en bdd existe déjà tel quel
     */
    public function check_existing_contact(array $contact_data)
    {
        global $wpdb;

        $existing_contact_id = $wpdb->get_results("SELECT ct.id 
        FROM {$wpdb->prefix}tp_contact ct
        WHERE (ct.email = '" . $contact_data['email'] . "' 
        AND ct.phone = '" . $contact_data['phone'] . "' 
        AND ct.street = '" . $contact_data['street'] . "' 
        AND ct.city = '" . $contact_data['city'] . "' 
        AND ct.postal_code = '" . $contact_data['postal_code'] . "');");

        echo "Existing contact :<br>";
        var_dump($existing_contact_id[0]->id);
        return $existing_contact_id[0]->id;
    }

    #endregion

    #region clubs

    /**
     * Récupère les informations nécéssaires au listing des clubs
     */
    public function get_clubs_data()
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT cl.name, ct.email, ct.phone, d.domain_name FROM {$wpdb->prefix}tp_clubs cl JOIN {$wpdb->prefix}tp_contact ct ON cl.contact_id = ct.id JOIN {$wpdb->prefix}tp_domains d ON cl.domain_id = d.id;");

        return $res;
    }

    /**
     *  Ne récupère que les informations nécéssaires au listing des clubs dans le
     *  formulaire d'enregistrement de participants pour les options du select
     */
    public function get_clubs_names_and_ids()
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT cl.name, cl.id FROM {$wpdb->prefix}tp_clubs cl;");

        return $res;
    }

    /**
     * Enregistre un club et son contact si son nom (du club) n'existe pas déjà
     */
    public function save_club()
    {
        global $wpdb;

        $row = $wpdb->get_row("SELECT id FROM {$wpdb->prefix}tp_club WHERE name='" . $_POST['name'] . "';");
        if (is_null($row)) {

            $valeurs_contact = [
                'street' => $_POST['address-street'],
                'city' => $_POST['address-city'],
                'postal_code' => $_POST['address-pc'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone']
            ];

            $shared_contact_id = $this->check_existing_contact($valeurs_contact);

            if ($shared_contact_id == false) {
                $wpdb->insert("{$wpdb->prefix}tp_contact", $valeurs_contact);

                $valeurs_club = [
                    'name' => $_POST['name'],
                    'domain_id' => $_POST['domain'],
                    'contact_id' => $wpdb->insert_id
                ];
                $wpdb->insert("{$wpdb->prefix}tp_clubs", $valeurs_club);
                echo "Nouveau contact ajouté.<br>";
            } else {
                $valeurs_club = [
                    'name' => $_POST['name'],
                    'domain_id' => $_POST['domain'],
                    'contact_id' => $shared_contact_id
                ];
                $wpdb->insert("{$wpdb->prefix}tp_clubs", $valeurs_club);
                echo "Le contact déjà existant a bien été attribué mais pas dupliqué dans la base de données.<br>";
            }
        } else {
            $_POST['sent'] = 'nope';
            echo "Quelque chose n'a pas fonctionné. Il semblerait que ce club existe déjà dans la base de données.<br>";
        }
    }

    /**
     * Supprime un club de la base de données, ainsi que son contact s'il n'est pas
     * rattaché à une autre source (autre club, participant)
     */
    public function delete_club()
    {
        global $wpdb;

        $name = $_POST['delete'];

        $contact_id = $wpdb->get_results("SELECT cl.contact_id FROM {$wpdb->prefix}tp_clubs cl WHERE cl.name = '$name';");

        $id = $contact_id[0]->contact_id;

        $wpdb->query("DELETE FROM wp_tp_clubs WHERE name='$name';");

        // Après avoir passé plusieurs heures à essayer de faire fonctionner la détection de contact déjà existant en une seule query (voir
        // commentaire suivant), sans succès, j'ai décidé de le faire comme ca :
        $shared_contact = $wpdb->get_results("SELECT cl.id FROM {$wpdb->prefix}tp_clubs cl WHERE cl.contact_id = '$id';");

        if (!$shared_contact) {
            $shared_contact = $wpdb->get_results("SELECT a.id FROM {$wpdb->prefix}tp_attendees a WHERE a.contact_id = '$id';");
        }
        // ancienne query seule :
        // $shared_contact = $wpdb->get_results("SELECT cl.id clid, a.id aid FROM {$wpdb->prefix}tp_clubs cl LEFT JOIN {$wpdb->prefix}tp_attendees a ON a.contact_id = '$id' WHERE cl.contact_id = '$id';");
        // Le problème étant que selon que le contact déjà existant soit attribué à un club ou un participant et selon l'ordre d'appel
        // des tables dans la query, soit le left join soit le right join ne fonctionnaient pas.

        if (!$shared_contact) {
            echo "Query de suppression de contact atteinte.<br>";

            $wpdb->query("DELETE FROM {$wpdb->prefix}tp_contact WHERE id = '$id';");
        }
    }

    #endregion

    #region attendees

    /**
     * Enregistre un participant et son contact si son nom et son prénom n'existent pas déjà (meh)
     */
    public function save_attendee()
    {
        global $wpdb;

        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $row = $wpdb->get_row("SELECT id FROM {$wpdb->prefix}tp_attendees WHERE firstname='$firstname' AND lastname='$lastname';");

        if (is_null($row)) {
            $valeurs_contact = [
                'street' => $_POST['address-street'],
                'city' => $_POST['address-city'],
                'postal_code' => $_POST['address-pc'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone']
            ];

            $shared_contact_id = $this->check_existing_contact($valeurs_contact);

            if (!$shared_contact_id) {
                $wpdb->insert("{$wpdb->prefix}tp_contact", $valeurs_contact);
                $valeurs_attendee = [
                    'firstname' => $_POST['firstname'],
                    'lastname' => $_POST['lastname'],
                    'club_id' => $_POST['club'],
                    'contact_id' => $wpdb->insert_id
                ];
                echo "Nouveau contact ajouté.<br>";
            } else {
                $valeurs_attendee = [
                    'firstname' => $_POST['firstname'],
                    'lastname' => $_POST['lastname'],
                    'club_id' => $_POST['club'],
                    'contact_id' => $shared_contact_id
                ];
                echo "Le contact déjà existant a bien été attribué mais pas dupliqué dans la base de données.<br>";
            }

            $wpdb->insert("{$wpdb->prefix}tp_attendees", $valeurs_attendee);
        } else {
            $_POST['sent'] = 'nope';
            echo "NOPE";
        }
    }

    /**
     * Récupère les informations nécéssaires au listing des participants
     */
    public function get_attendees_data()
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT a.id, a.firstname, a.lastname, cl.name, ct.email, ct.phone, ct.city FROM {$wpdb->prefix}tp_attendees a JOIN {$wpdb->prefix}tp_contact ct ON a.contact_id = ct.id JOIN {$wpdb->prefix}tp_clubs cl ON a.club_id=cl.id;");

        return $res;
    }

    /**
     * Supprime un participant de la base de données, ainsi que son contact s'il n'est pas
     * rattaché à une autre source (club, autre participant)
     */
    public function delete_attendee()
    {
        global $wpdb;

        $aid = $_POST['delete'];

        $contact_id = $wpdb->get_results("SELECT a.contact_id FROM {$wpdb->prefix}tp_attendees a WHERE a.id = '$aid';");

        $cid = $contact_id[0]->contact_id;


        // Après avoir passé plusieurs heures à essayer de faire fonctionner la détection de contact déjà existant en une seule query (voir 
        // commentaire suivant), sans succès, j'ai décidé de le faire comme ca :
        $shared_contact = $wpdb->get_results("SELECT cl.id FROM {$wpdb->prefix}tp_clubs cl WHERE cl.contact_id = '$cid';");

        if (!$shared_contact) {
            $shared_contact = $wpdb->get_results("SELECT a.id FROM {$wpdb->prefix}tp_attendees a WHERE a.contact_id = '$cid';");
        }
        // ancienne query seule :
        // $shared_contact = $wpdb->get_results("SELECT cl.id clid, a.id aid FROM {$wpdb->prefix}tp_clubs cl LEFT JOIN {$wpdb->prefix}tp_attendees a ON a.contact_id = '$cid' WHERE cl.contact_id = '$cid';");
        // Le problème étant que selon que le contact déjà existant soit attribué à un club ou un participant et selon l'ordre d'appel
        // des tables dans la query, soit le left join soit le right join ne fonctionnaient pas.

        $wpdb->query("DELETE FROM {$wpdb->prefix}tp_attendees WHERE id = $aid;");

        if (!$shared_contact) {
            echo "Query de suppression de contact atteinte.<br>";

            $wpdb->query("DELETE FROM {$wpdb->prefix}tp_contact WHERE id = '$cid';");
        }
    }

    #endregion
}
