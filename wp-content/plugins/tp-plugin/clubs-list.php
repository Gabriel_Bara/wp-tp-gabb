<?php


require_once "/app/wp-admin/includes/class-wp-list-table.php";
require_once plugin_dir_path(__FILE__) . "/service/Tp_Db_Service.php";

class ClubsList extends WP_List_Table
{
    private $dal;

    public function __construct($args = array())
    {
        parent::__construct([
            'singular' => __("Club"),
            'plural' => __("Clubs")
        ]);

        $this->dal = new Tp_Db_Service();
    }

    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        // pagination
        $perPage = $this->get_items_per_page('clubs_per_page', 10);
        $currentPage = $this->get_pagenum();

        //données
        $data = $this->dal->get_clubs_data();
        $totalPage = count($data);

        //tri
        usort($data, array(&$this, 'usort_reorder'));
        $paginationData = array_slice($data, (($currentPage - 1) * $perPage), $perPage);

        $this->set_pagination_args([
            'total-item' => $totalPage,
            'per-page' => $perPage
        ]);

        $this->_column_headers = [$columns, $hidden, $sortable];
        $this->items = $paginationData;
    }

    protected function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'name':
            case 'domain_name':
            case 'email':
            case 'phone':
                return $item->$column_name;
            case 'suppr':
                return '<form method="post"><input type="hidden" name="delete" value="' . $item->name . '"><input type="submit" value="❌"></form>';
                break;
            default:
                return print_r($item, true);
        }
    }

    public function usort_reorder($a, $b)
    {
        $orderBy = (!empty($_GET['orderby'])) ? $_GET['orderby'] : 'id';
        $order = (!empty($_GET['order'])) ? $_GET['order'] : 'desc';
        $result = strcmp($a->$orderBy, $b->$orderBy);
        return ($order === 'asc') ? $result : -$result;
    }

    public function get_columns()
    {
        $columns = [
            'name' => 'Nom',
            'domain_name' => 'Domaine',
            'email' => 'Email',
            'phone' => 'Téléphone',
            'suppr' => 'Supprimer'
        ];
        return $columns;
    }

    public function get_hidden_columns(): array
    {
        return [];
    }

    public function get_sortable_columns()
    {
        return [
            'name' => ['name', true],
            'domain_name' => ['domain_name', false],
            'email' => ['email', false],
            'phone' => ['phone', false]
        ];
    }
}
