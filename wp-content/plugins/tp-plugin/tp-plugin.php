<?php

require_once plugin_dir_path(__FILE__) . "service/Tp_Db_Service.php";
require_once plugin_dir_path(__FILE__) . "clubs-list.php";
require_once plugin_dir_path(__FILE__) . "attendees-list.php";

/**
 * Plugin Name: TP Plugin
 * Description: Plugin qui servira à enregistrer une fonctionnalité pour le TP
 * Author: Gabriel Bara
 * Version: 1.0
 */

class TpPlugin
{
    public function __construct()
    {

        // activation du plugin : création des tables
        register_activation_hook(__FILE__, array('Tp_Db_Service', 'create_db'));

        // désactivation du plugin : je vide la table
        register_deactivation_hook(__FILE__, array('Tp_Db_Service', 'empty_db'));

        register_uninstall_hook(__File__, array('Tp_Db_Service', 'drop_db'));

        add_action("admin_menu", [$this, "add_menu_clubs"]);
    }

    public function add_menu_clubs()
    {
        add_menu_page("Les clubs de l'association", "Clubs", "manage_options", "getClubs", [$this, 'get_clubs'], "dashicons-groups", 48);
        add_submenu_page("getClubs", "Ajouter un club", "Ajouter club", "manage_options", "addClub", [$this, 'add_club']);
        add_submenu_page("getClubs", "Liste des participants", "Participants", "manage_options", "getAttendees", [$this, 'get_attendees']);
        add_submenu_page("getClubs", "Ajouter un participant", "Ajouter participant", "manage_options", "addAttendee", [$this, 'add_attendee']);
    }

    public function get_clubs()
    {
        if (isset($_POST['delete'])) {
            $db_service = new Tp_Db_Service;
            $db_service->delete_club();
        }

        $table = new ClubsList();
        $table->prepare_items();
        echo $table->display();
    }

    public function add_club()
    {
        $db_service = new Tp_Db_Service;

        if (isset($_POST['sent'])) {
            $db_service->save_club();
            if ($_POST['sent'] == 'yep') {
?>
                Le club a bien été ajouté !<br>
                Retour à la <a href="admin.php?page=getClubs">liste des clubs</a>.
            <?php
            } else {
            ?>
                Ce club existe déjà dans la base de données.<br>
                <a href="admin.php?page=addClub">Retour au formulaire</a> / <a href="admin.php?page=getClubs">Liste des clubs</a>
            <?php
            }
        } else {
            echo '<h2>' . get_admin_page_title() . '</h2>';
            ?>
            <form method="post">
                <input type="hidden" name="sent" value="yep">
                <div>
                    <label for="name"><span style="font-weight: 700;">Nom : </span></label>
                    <input type="text" name="name" id="name" class="widefat" required>
                </div><br>
                <div>
                    <label for="email"><span style="font-weight: 700;">Email : </span></label>
                    <input type="text" name="email" id="email" class="widefat" required>
                </div><br>
                <div>
                    <label for="phone"><span style="font-weight: 700;">N° de téléphone : </span></label>
                    <input type="text" name="phone" id="phone" class="widefat" required>
                </div><br>
                <div><span style="font-weight: 700;">Adresse : </span><br>
                    <div>
                        <label for="address-street">Rue : <br></label>
                        <input type="text" name="address-street" id="address-street">
                    </div>
                    <div>
                        <label for="address-city">Ville : <br></label>
                        <input type="text" name="address-city" id="address-city">
                    </div>
                    <div>
                        <label for="address-pc">Code postal : <br></label>
                        <input type="text" name="address-pc" id="address-pc">
                    </div>
                </div><br>
                <div>
                    <label for="domain">Domaine de prédilection : </label>
                    <select name="domain" id="domain">
                        <option value="1">Drones</option>
                        <option value="2">Automobiles</option>
                        <option value="3">Bateaux</option>
                    </select>
                </div><br>
                <div>
                    <input type="submit" value="Enregistrer">
                </div>
            </form>
            <?php
        }
    }

    public function add_attendee()
    {
        $db_service = new Tp_Db_Service;
        $clubs = $db_service->get_clubs_names_and_ids();
        if (isset($_POST['sent'])) {
            $db_service->save_attendee();
            if ($_POST['sent'] == 'yep') {
            ?>
                Le participant a bien été enregistré !<br>
                Retour à la <a href="admin.php?page=getAttendees">liste des participants</a>.
            <?php
            } else {
            ?>
                Ce participant existe déjà dans la base de données.<br>
                <a href="admin.php?page=addAttendee">Retour au formulaire</a> / <a href="admin.php?page=getAttendees">Liste des participants</a>
            <?php
            }
        } else {
            echo '<h2>' . get_admin_page_title() . '</h2>';
            ?>
            <form method="post">
                <input type="hidden" name="sent" value="yep">
                <div>
                    <label for="lastname"><span style="font-weight: 700;">Nom : </span></label>
                    <input type="text" name="lastname" id="lastname" class="widefat" required>
                </div><br>
                <div>
                    <label for="firstname"><span style="font-weight: 700;">Prenom : </span></label>
                    <input type="text" name="firstname" id="firstname" class="widefat" required>
                </div><br>
                <div>
                    <label for="email"><span style="font-weight: 700;">Email : </span></label>
                    <input type="text" name="email" id="email" class="widefat" required>
                </div><br>
                <div>
                    <label for="phone"><span style="font-weight: 700;">N° de téléphone : </span></label>
                    <input type="text" name="phone" id="phone" class="widefat" required>
                </div><br>
                <div><span style="font-weight: 700;">Adresse : </span><br>
                    <div>
                        <label for="address-street">Rue : <br></label>
                        <input type="text" name="address-street" id="address-street">
                    </div>
                    <div>
                        <label for="address-city">Ville : <br></label>
                        <input type="text" name="address-city" id="address-city">
                    </div>
                    <div>
                        <label for="address-pc">Code postal : <br></label>
                        <input type="text" name="address-pc" id="address-pc">
                    </div>
                </div><br>
                <div>
                    <label for="club">Club : </label>
                    <select name="club" id="club">
                        <?php foreach ($clubs as $club) {
                            echo '<option value="' . $club->id . '">' . $club->name . '</option>';
                        }
                        ?>
                    </select>
                </div><br>
                <div>
                    <input type="submit" value="Enregistrer">
                </div>
            </form>

<?php
        }
    }

    public function get_attendees()
    {
        if (isset($_POST['delete'])) {
            $db_service = new Tp_Db_Service;
            $db_service->delete_attendee();
        }

        $table = new AttendeesList();
        $table->prepare_items();
        echo $table->display();
    }
}

new TpPlugin;
